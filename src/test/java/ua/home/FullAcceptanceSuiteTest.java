package ua.home;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ua.home.categories.Buggy;
import ua.home.categories.FullAcceptance;
import ua.home.features.TodosE2ETest;
import ua.home.features.TodosOperationsAtAllFilterTest;

@RunWith(Categories.class)
@Categories.IncludeCategory(FullAcceptance.class)
@Categories.ExcludeCategory(Buggy.class)
@Suite.SuiteClasses({TodosE2ETest.class, TodosOperationsAtAllFilterTest.class})

public class FullAcceptanceSuiteTest {
}
