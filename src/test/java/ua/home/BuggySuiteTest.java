package ua.home;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ua.home.categories.Buggy;
import ua.home.features.TodosE2ETest;
import ua.home.features.TodosOperationsAtAllFilterTest;

@RunWith(Categories.class)
@Categories.IncludeCategory(Buggy.class)
@Suite.SuiteClasses({TodosOperationsAtAllFilterTest.class, TodosE2ETest.class})

public class BuggySuiteTest {

//    public static void test() {
//        try{
//
//        } catch (NoTestsRemainException e) {
//            System.out.println("There are no bugs in BuggySuite.");
//        }
//    }

}
