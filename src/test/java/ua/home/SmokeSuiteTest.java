package ua.home;


import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ua.home.categories.FullAcceptance;
import ua.home.categories.Smoke;
import ua.home.features.TodosE2ETest;

@RunWith(Categories.class)
@Categories.IncludeCategory(Smoke.class)
//@Categories.ExcludeCategory(FullAcceptance.class)
@Suite.SuiteClasses({TodosE2ETest.class})

public class SmokeSuiteTest {
}
