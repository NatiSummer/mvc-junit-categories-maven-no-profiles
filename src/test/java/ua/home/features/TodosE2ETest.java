package ua.home.features;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import ua.home.categories.FullAcceptance;
import ua.home.categories.Smoke;

import static ua.home.BasePage.*;
import static ua.home.BasePage.assertItemsLeft;
import static ua.home.BasePage.assertVisibleTaskTexts;

public class TodosE2ETest extends BaseTest{
    @Test
    @Category({Smoke.class, FullAcceptance.class})
    public void lifeCycle(){
        addTasks("t1", "t2", "t3", "t4");
        assertTasksTexts("t1", "t2", "t3", "t4");
        assertItemsLeft(4);

        delete("t2");
        assertTasksTexts("t1", "t3", "t4");
        assertItemsLeft(3);

        toggleTask("t4");
        clearCompleted();
        assertTasksTexts("t1", "t3");
        assertItemsLeft(2);

        edit("t1", "t1 - edited");
        assertTasksTexts("t1 - edited", "t3");

        toggleTask("t1 - edited");
        assertItemsLeft(1);
        toggleTask("t1 - edited");
        assertItemsLeft(2);

        // complete task
        toggleTask("t1 - edited");
        assertItemsLeft(1);

        // verify filtering at Active tab
        openActive();
        assertVisibleTaskTexts("t3");
        assertItemsLeft(1);

        // verify filtering at Completed tab
        openCompleted();
        assertVisibleTaskTexts("t1 - edited");
        assertItemsLeft(1);
    }
}

